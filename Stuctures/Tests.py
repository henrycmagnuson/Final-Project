'''
@author: Henry
'''
import unittest
import Bet
import User
import Match
import CoinTypes




##testing creation and a bit of manipulation on the basic classes
class Test(unittest.TestCase):


    def test_name(self):
        user1 = User("Henry", 21, "Ethereum", 5)
        self.assertEqual(user1.name, "Henry")
        
    def test_age(self):
        user1 = User("Henry", 21, "Ethereum", 5)
        self.assertEqual(user1.age, 21)
        
    def test_coin(self):
        user1 = User("Henry", 21, "Ethereum", 5)
        self.assertEqual(user1.coinType, "Ethereum")
        
    def test_amount(self):
        user1 = User("Henry", 21, "Ethereum", 5)
        self.assertEqual(user1.coinAmount, 5)
        
    def test_addCoin(self):
        user1 = User("Henry", 21, "Ethereum", 5)
        user1.addCoin(5)
        self.assertEqual(user1.coinAmount, 10)
        
    def test_bet(self):
        bet1 = Bet("Henry", "Bears", "Bitcoin", 2)
        self.assertEqual(bet1.user, "Henry")
        
    def test_betTeam(self):
        bet1 = Bet("Henry", "Bears", "Bitcoin", 2)
        self.assertEqual(bet1.team, "Bears")
        
    def test_betCoin(self):
        bet1 = Bet("Henry", "Bears", "Bitcoin", 2)
        self.assertEqual(bet1.coinType, "Bitcoin")
        
    def test_betAmount(self):
        bet1 = Bet("Henry", "Bears", "Bitcoin", 2)
        self.assertEqual(bet1.coinAmount, 2)
        
    def test_match(self):
        match1 = Match("Henry", "John", "Bears", "Packers", "Bitcoin", "Ethereum", 2)
        self.assertEqual(match1.user1, "Henry")
        
    def test_match2(self):
        match1 = Match("Henry", "John", "Bears", "Packers", "Bitcoin", "Ethereum", 2)
        self.assertEqual(match1.user2, "John")
        
    def test_matchTeam1(self):
        match1 = Match("Henry", "John", "Bears", "Packers", "Bitcoin", "Ethereum", 2)
        self.assertEqual(match1.team1, "Bears")
        
    def test_matchTeam2(self):
        match1 = Match("Henry", "John", "Bears", "Packers", "Bitcoin", "Ethereum", 2)
        self.assertEqual(match1.team2, "Packers")
        
    def test_matchCoin1(self):
        match1 = Match("Henry", "John", "Bears", "Packers", "Bitcoin", "Ethereum", 2)
        self.assertEqual(match1.coinType1, "Bitcoin")
        
    def test_matchCoin2(self):
        match1 = Match("Henry", "John", "Bears", "Packers", "Bitcoin", "Ethereum", 2)
        self.assertEqual(match1.coinType2, "Ethereum")
        
    def test_matchCoinAmount(self):
        match1 = Match("Henry", "John", "Bears", "Packers", "Bitcoin", "Ethereum", 2)
        self.assertEqual(match1.coinAmount1, 2)
        
        
        
        
        
        
        



        
class User:
    def __init__(self, name, age, coinType, coinAmount):
        self.name = name
        self.age = age 
        self.coinType = coinType
        self.coinAmount = coinAmount
    
    
    def addCoin(self, amount):
        self.coinAmount += amount
        
    def changeCoinType(self, newCoinType):
        self.coinAmount = self.coinAmount*((self.coinType.USDvalue)/newCoinType.USDvalue)
        self.coinType = newCoinType
        
        
class Bet:
    def __init__(self, user ,team, coinType, coinAmount):
        self.user = user
        self.team = team
        self.coinType = coinType
        self.coinAmount = coinAmount
        
class Match:
    def __init__(self, user1 , user2, team1 , team2,  coinType1, coinType2, coinAmount1):
        self.user1 = user1
        self.user2 = user2
        self.team1 = team1
        self.team2 = team2 
        self.coinType1 = coinType1
        self.coinType2 = coinType2
        self.coinAmount1 = coinAmount1        
        
        